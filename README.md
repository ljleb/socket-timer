# Input support

- stdin
- socket

# Commands

- `'exit'`, `''`: exit the app
- `'time'`: update timer
- `'print'`: prints to stdout the time elapsed between the last two 'time' calls

# Usage

    $ python3 src/timer.py
    time
    time
    print
    1.9572196006774902
    exit
    $ 